:free-space: Avoid deleting deleting incomplete files from under newly added download
	     items while identifying orphans.
