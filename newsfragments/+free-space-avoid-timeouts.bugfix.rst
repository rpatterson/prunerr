:free-space: Prevent download clients under heavy load from blocking the freeing of disk
             space.
