:free-space: Also consider files in the Transmission ``incomplete-dir`` when identifying
	     item files as orphans to delete.
