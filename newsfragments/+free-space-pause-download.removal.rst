:free-space: Remove support for pausing/resuming download out of Prunerr and into the
             download client container via a cron job.
