:free-space: Tolerate but log when a previous ``$ prunerr free-space`` run deleted the
             files but the download client, under heavy load, fails to remove the item.
