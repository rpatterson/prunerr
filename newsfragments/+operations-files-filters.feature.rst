:Operations: Make the ``type: "files"`` operation more flexible to support filtering on
             file attributes. Needed to reproduce the ``size_imported`` use case but
             only on wanted/selected files.
