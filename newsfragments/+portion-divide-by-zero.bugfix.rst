:operations: Avoid division by zero when no files are selected and using the ``portion``
             option for the item files operation results.
