:Re-add: Add a new ``re-add`` sub-command to remove and re-add all seeding download
         items with nothing downloaded. This is useful to avoid re-verifying seeding
         items when Transmission loses track of the items progress such as when it's
         ``/config/resume/*.resume`` files are lost or recreated.
